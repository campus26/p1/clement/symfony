<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class UserFixtures extends Fixture
{

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 4; $i++) {
        $user = new User();
        $user->setEmail("exemple".$i."@gmail.com");
        $roles=['ROLE_USER'];
    $user->setRoles($roles);
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'mdp'.$i
        ));

         $manager->persist($user);
    }

    $user = new User();
    $user->setEmail("admin@gmail.com");
    $user->setPassword($this->passwordEncoder->encodePassword($user,'admin'));
    $roles=['ROLE_USER','ROLE_ADMIN'];
    $user->setRoles($roles);
    $manager->persist($user);
    $manager->flush();
    }
}
