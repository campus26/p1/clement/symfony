<?php

namespace App\Entity;

use App\Repository\TypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeRepository::class)
 */
class Type
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titleType;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $DescriptionType;

    /**
     * @ORM\ManyToMany(targetEntity=Game::class, mappedBy="types")
     */
    private $Game;

    public function __construct()
    {
        $this->Game = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitleType(): ?string
    {
        return $this->titleType;
    }

    public function setTitleType(string $titleType): self
    {
        $this->titleType = $titleType;

        return $this;
    }

    public function getDescriptionType(): ?string
    {
        return $this->DescriptionType;
    }

    public function setDescriptionType(?string $DescriptionType): self
    {
        $this->DescriptionType = $DescriptionType;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGame(): Collection
    {
        return $this->Game;
    }

    public function addGame(Game $game): self
    {
        if (!$this->Game->contains($game)) {
            $this->Game[] = $game;
            $game->addType($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        $this->Game->removeElement($game);

        return $this;
    }
}
