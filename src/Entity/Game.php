<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titleGame;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $descriptionGame;

    /**
     * @ORM\Column(type="date")
     */
    private $releasedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageGame;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbSellGame;

    /**
     * @ORM\ManyToMany(targetEntity=Type::class, inversedBy="Game")
     */
    private $types;

    /**
     * @ORM\ManyToMany(targetEntity=Platform::class, inversedBy="Game")
     */
    private $platforms;

    /**
     * @ORM\OneToMany(targetEntity=UserHasGame::class, mappedBy="Game")
     */
    private $userHasGames;

    public function __construct()
    {
        $this->types = new ArrayCollection();
        $this->platforms = new ArrayCollection();
        $this->userHasGames = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitleGame(): ?string
    {
        return $this->titleGame;
    }

    public function setTitleGame(string $titleGame): self
    {
        $this->titleGame = $titleGame;

        return $this;
    }

    public function getDescriptionGame(): ?string
    {
        return $this->descriptionGame;
    }

    public function setDescriptionGame(string $descriptionGame): self
    {
        $this->descriptionGame = $descriptionGame;

        return $this;
    }

    public function getReleasedAt(): ?\DateTimeInterface
    {
        return $this->releasedAt;
    }

    public function setReleasedAt(\DateTimeInterface $releasedAt): self
    {
        $this->releasedAt = $releasedAt;

        return $this;
    }

    public function getImageGame(): ?string
    {
        return $this->imageGame;
    }

    public function setImageGame(?string $imageGame): self
    {
        $this->imageGame = $imageGame;

        return $this;
    }

    public function getNbSellGame(): ?int
    {
        return $this->nbSellGame;
    }

    public function setNbSellGame(?int $nbSellGame): self
    {
        $this->nbSellGame = $nbSellGame;

        return $this;
    }

    /**
     * @return Collection|Type[]
     */
    public function getTypes(): Collection
    {
        return $this->types;
    }

    public function addType(Type $type): self
    {
        if (!$this->types->contains($type)) {
            $this->types[] = $type;
            $type->addGame($this);
        }

        return $this;
    }

    public function removeType(Type $type): self
    {
        if ($this->types->removeElement($type)) {
            $type->removeGame($this);
        }

        return $this;
    }

    /**
     * @return Collection|Platform[]
     */
    public function getPlatforms(): Collection
    {
        return $this->platforms;
    }

    public function addPlatform(Platform $platform): self
    {
        if (!$this->platforms->contains($platform)) {
            $this->platforms[] = $platform;
            $platform->addGame($this);
        }

        return $this;
    }

    public function removePlatform(Platform $platform): self
    {
        if ($this->platforms->removeElement($platform)) {
            $platform->removeGame($this);
        }

        return $this;
    }

    /**
     * @return Collection|UserHasGame[]
     */
    public function getUserHasGames(): Collection
    {
        return $this->userHasGames;
    }

    public function addUserHasGame(UserHasGame $userHasGame): self
    {
        if (!$this->userHasGames->contains($userHasGame)) {
            $this->userHasGames[] = $userHasGame;
            $userHasGame->setGame($this);
        }

        return $this;
    }

    public function removeUserHasGame(UserHasGame $userHasGame): self
    {
        if ($this->userHasGames->removeElement($userHasGame)) {
            // set the owning side to null (unless already changed)
            if ($userHasGame->getGame() === $this) {
                $userHasGame->setGame(null);
            }
        }

        return $this;
    }
}
