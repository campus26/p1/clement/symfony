<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210414091446 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_has_game ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_has_game ADD CONSTRAINT FK_9EFA0ED3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_9EFA0ED3A76ED395 ON user_has_game (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_has_game DROP FOREIGN KEY FK_9EFA0ED3A76ED395');
        $this->addSql('DROP INDEX IDX_9EFA0ED3A76ED395 ON user_has_game');
        $this->addSql('ALTER TABLE user_has_game DROP user_id');
    }
}
