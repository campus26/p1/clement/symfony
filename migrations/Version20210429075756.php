<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210429075756 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, title_game VARCHAR(255) NOT NULL, description_game VARCHAR(512) NOT NULL, released_at DATE NOT NULL, image_game VARCHAR(255) DEFAULT NULL, nb_sell_game INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE platform (id INT AUTO_INCREMENT NOT NULL, name_plat VARCHAR(255) NOT NULL, description_plat LONGTEXT NOT NULL, logo_plat VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE platform_game (platform_id INT NOT NULL, game_id INT NOT NULL, INDEX IDX_A72356A0FFE6496F (platform_id), INDEX IDX_A72356A0E48FD905 (game_id), PRIMARY KEY(platform_id, game_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type (id INT AUTO_INCREMENT NOT NULL, title_type VARCHAR(255) NOT NULL, description_type VARCHAR(512) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_game (type_id INT NOT NULL, game_id INT NOT NULL, INDEX IDX_F424F484C54C8C93 (type_id), INDEX IDX_F424F484E48FD905 (game_id), PRIMARY KEY(type_id, game_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_has_game (id INT AUTO_INCREMENT NOT NULL, game_id INT DEFAULT NULL, user_id INT NOT NULL, owned_date DATE NOT NULL, INDEX IDX_9EFA0ED3E48FD905 (game_id), INDEX IDX_9EFA0ED3A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_has_game_platform (user_has_game_id INT NOT NULL, platform_id INT NOT NULL, INDEX IDX_C8F0E8C12A65784B (user_has_game_id), INDEX IDX_C8F0E8C1FFE6496F (platform_id), PRIMARY KEY(user_has_game_id, platform_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE platform_game ADD CONSTRAINT FK_A72356A0FFE6496F FOREIGN KEY (platform_id) REFERENCES platform (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE platform_game ADD CONSTRAINT FK_A72356A0E48FD905 FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE type_game ADD CONSTRAINT FK_F424F484C54C8C93 FOREIGN KEY (type_id) REFERENCES type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE type_game ADD CONSTRAINT FK_F424F484E48FD905 FOREIGN KEY (game_id) REFERENCES game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_has_game ADD CONSTRAINT FK_9EFA0ED3E48FD905 FOREIGN KEY (game_id) REFERENCES game (id)');
        $this->addSql('ALTER TABLE user_has_game ADD CONSTRAINT FK_9EFA0ED3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_has_game_platform ADD CONSTRAINT FK_C8F0E8C12A65784B FOREIGN KEY (user_has_game_id) REFERENCES user_has_game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_has_game_platform ADD CONSTRAINT FK_C8F0E8C1FFE6496F FOREIGN KEY (platform_id) REFERENCES platform (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE platform_game DROP FOREIGN KEY FK_A72356A0E48FD905');
        $this->addSql('ALTER TABLE type_game DROP FOREIGN KEY FK_F424F484E48FD905');
        $this->addSql('ALTER TABLE user_has_game DROP FOREIGN KEY FK_9EFA0ED3E48FD905');
        $this->addSql('ALTER TABLE platform_game DROP FOREIGN KEY FK_A72356A0FFE6496F');
        $this->addSql('ALTER TABLE user_has_game_platform DROP FOREIGN KEY FK_C8F0E8C1FFE6496F');
        $this->addSql('ALTER TABLE type_game DROP FOREIGN KEY FK_F424F484C54C8C93');
        $this->addSql('ALTER TABLE user_has_game DROP FOREIGN KEY FK_9EFA0ED3A76ED395');
        $this->addSql('ALTER TABLE user_has_game_platform DROP FOREIGN KEY FK_C8F0E8C12A65784B');
        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE platform');
        $this->addSql('DROP TABLE platform_game');
        $this->addSql('DROP TABLE type');
        $this->addSql('DROP TABLE type_game');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_has_game');
        $this->addSql('DROP TABLE user_has_game_platform');
    }
}
