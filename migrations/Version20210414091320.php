<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210414091320 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user_has_game_user');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_has_game_user (user_has_game_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_A9AD857A2A65784B (user_has_game_id), INDEX IDX_A9AD857AA76ED395 (user_id), PRIMARY KEY(user_has_game_id, user_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE user_has_game_user ADD CONSTRAINT FK_A9AD857A2A65784B FOREIGN KEY (user_has_game_id) REFERENCES user_has_game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_has_game_user ADD CONSTRAINT FK_A9AD857AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }
}
